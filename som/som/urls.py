from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('SOMAPI/', include('SOMAPI.urls')),
    path('admin/', admin.site.urls),
]
