from django.contrib import admin
from .models import *


class StatusAdmin(admin.ModelAdmin):
    pass


class ObjectAdmin(admin.ModelAdmin):
    pass


admin.site.register(Status, StatusAdmin)
admin.site.register(Object, ObjectAdmin)
