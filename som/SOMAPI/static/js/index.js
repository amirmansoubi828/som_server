import React from 'react'
import ReactDOM from 'react-dom'
import {ListGroup , Accordion , Card} from 'react-bootstrap'

class Main extends React.Component {

  render()
    {   
        var list = window.props;
        console.log(list)

        return <div>
            <ListGroup>
            {list.map(function(item){
                return <ListGroup.Item>
                    <h4>{item["title"]} ({item["objects"].length})</h4>
                    <ListGroup>
                        {item["objects"].map(function(obj){
                            return <ListGroup.Item variant="primary">
                            <Accordion defaultActiveKey="1">
                                <Card>
                                    <Card.Header>
                                    <Accordion.Toggle as={Card.Header} eventKey="0">
                                        id : {obj.id}
                                    </Accordion.Toggle>
                                    </Card.Header>
                                    <Accordion.Collapse eventKey="0">
                                    <Card.Body>
                                        <p>last staff : {obj.last_staff}</p>
                                        <p>last edit time : {obj.last_edit_time}</p>
                                        </Card.Body>
                                    </Accordion.Collapse>
                                </Card>
                            </Accordion>
                            </ListGroup.Item>
                        })}
                    </ListGroup>
                </ListGroup.Item>
            })}
            </ListGroup>
        </div>
    }
}





ReactDOM.render(
    <Main/>, 
    window.react_mount, 
);