from django.urls import path

from . import views

urlpatterns = [
    path('all', views.values, name='values'),  # status
    path('add', views.add, name='add'),
    path('info/', views.info, name='info'),
    path('change', views.change, name='change'),
    path('dashboard', views.dashboard, name='dashboard'),
    path('objectsall', views.objectsall, name='objectsall'),
]
