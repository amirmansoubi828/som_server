from django.db import models
import datetime


# Create your models here.


class Status(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    title = models.CharField(blank=True, max_length=255)

    def __str__(self):
        return self.title


class Object(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    status = models.ForeignKey(Status, on_delete=models.CASCADE, editable=True)
    last_staff = models.CharField(blank=True, max_length=255)
    last_edit_time = models.DateTimeField(default=datetime.datetime.now)

    def change_status(self, id, new_status_title, staff_name):
        self.id = id;
        self.status = Status.objects.get(title=new_status_title)
        self.last_edit_time = datetime.datetime.now()
        self.last_staff = staff_name
        self.save()

    def __str__(self):
        return "{} : {} : {}".format(self.id, self.status, self.last_edit_time.astimezone())
