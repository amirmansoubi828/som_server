from django.apps import AppConfig


class SomapiConfig(AppConfig):
    name = 'SOMAPI'
