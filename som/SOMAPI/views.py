from django.http import JsonResponse
from .models import *
import json
from django.shortcuts import render
from django.views import View
import datetime


def values(request):
    if request.method == "GET":
        status_list = []
        for status in Status.objects.all():
            status_list.append(status.title)
        test = {"status": status_list}
        return JsonResponse(test)


def info(request):
    if request.method == "GET":
        try:
            id = request.GET["id"]
            searched_object = Object.objects.get(id=id)
            return JsonResponse(
                {"id": id, "status": searched_object.status.title, "last_staff": searched_object.last_staff,
                 "last_edit_time": searched_object.last_edit_time.astimezone().strftime("%A, %d. %B %Y %I:%M %p")})
        except Exception as e:
            print(e)
            return JsonResponse({"id": "Error", "status": str(e)})


def change(request):
    if request.method == "POST":
        try:
            id = request.POST.get("id")
            new_status = request.POST.get("status")
            staff_name = request.POST.get("last_staff")
            old_object = Object.objects.get(id=int(id))
            if id != old_object.id or staff_name != old_object.last_staff or new_status != old_object.status.title:
                old_object.change_status(id, new_status, staff_name)
                return JsonResponse({"status": "Success", "detail": "ok"})
            else:
                return JsonResponse({"status": "Error", "detail": "No change"})
        except Exception as e:
            return JsonResponse({"status": "Error", "detail": str(e)})


def dashboard(request):
    objects_status_tuples = []
    context = {
        'props': []
    }
    objects_status = []
    for status in Status.objects.all():
        objects_status = list(Object.objects.filter(status=status))
        objects_status_tuples = []
        for obj in objects_status:
            objects_status_tuples.append({
                "id": obj.id,
                "last_staff": obj.last_staff,
                "last_edit_time": str(obj.last_edit_time.astimezone().strftime("%A, %d. %B %Y %I:%M %p"))
            })
        context['props'].append(
            {"title": status.title, "objects": objects_status_tuples})
    print(context['props'])
    return render(request, 'index.html', context)


def objectsall(request):
    objects = []
    for obj in Object.objects.all():
        objects.append({
            "id": obj.id,
            "status": obj.status.title,
            "last_staff": obj.last_staff,
            "last_edit_time": obj.last_edit_time.astimezone().strftime("%A, %d. %B %Y %I:%M %p")
        })
    return JsonResponse({"list": objects})


def add(request): #adding new
    if request.method == "POST":
        try:
            id = request.POST.get("id")
            if Object.objects.filter(id=id).exists():
                return JsonResponse({"status": "Error", "detail": "Exists in DB."})
            new_status = request.POST.get("status")
            staff_name = request.POST.get("last_staff")
            new_object = Object(id=id, status=Status.objects.get(
                title=new_status), last_staff=staff_name)
            new_object.save()
            return JsonResponse({"status": "Success", "detail": "ok"})
        except Exception as e:
            return JsonResponse({"status": "Error", "detail": str(e)})
